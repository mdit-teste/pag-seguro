<?php

namespace App\Http\Controllers\PagSeguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionListenerController extends Controller
{
    public function notification()
    {
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        try {
            if (\PagSeguro\Helpers\Xhr::hasPost()) {
                $response = \PagSeguro\Services\Transactions\Notification::check(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );
            } else {
                throw new \InvalidArgumentException($_POST);
            }
        
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
    }
}
