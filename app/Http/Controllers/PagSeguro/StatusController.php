<?php

namespace App\Http\Controllers\PagSeguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusController extends Controller
{
   /*****************
     * POR REFERENCIA
     */
    public function reference(Request $request)
    {
        \PagSeguro\Library::initialize();

        $options = [
            'initial_date' => '2019-04-01T14:55',
            // 'final_date' => '2019-04-24T09:55', //Optional
            'page' => 1, //Optional
            'max_per_page' => 20, //Optional
        ];
        
        $reference = $request['reference'];
        
        try {
            $response = \PagSeguro\Services\Transactions\Search\Reference::search(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $reference,
                $options
            );
        
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*****************
     * POR TRANSACTION_ID
     */
    public function transaction(Request $request)
    {
        \PagSeguro\Library::initialize();

        $code = $request['transaction'];
        
        try {
            $response = \PagSeguro\Services\Transactions\Search\Code::search(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $code
            );
        
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function data(Request $request)
    {
        \PagSeguro\Library::initialize();

        $options = [
            'initial_date' => '2019-08-01T14:55',
            'final_date' => '2019-08-27T09:55', //Optional
            'page' => 1, //Optional
            'max_per_page' => 20, //Optional
        ];
        
        try {
            $response = \PagSeguro\Services\Transactions\Search\Date::search(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $options
            );
        
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
