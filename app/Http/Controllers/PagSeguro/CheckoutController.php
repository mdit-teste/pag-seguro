<?php

namespace App\Http\Controllers\PagSeguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pedido;

class CheckoutController extends Controller
{

    /******************
     * REDIRECT
     */
    public function checkoutRedirect(Request $request)
    {
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        $payment = new \PagSeguro\Domains\Requests\Payment();
        
        $payment->addItems()->withParameters(
            '0001',
            'Notebook prata',
            1,
            130.00
        );
        
        $payment->addItems()->withParameters(
            '0002',
            'Notebook preto',
            2,
            430.00
        );
        
        $payment->setCurrency("BRL");
        
        $payment->setExtraAmount(11.5);
        
        $pedido = new Pedido([
            'codigo' => 'CHJPHP0000-'
        ]);
        $pedido->save();

        $pedido->codigo .= $pedido->id;
        $pedido->update();

        $referencia = $pedido->codigo;
        $payment->setReference($referencia);
        // $payment->setReference("LIBPHP000001");
        
        $payment->setRedirectUrl(route('checkout.pagseguro.pedido', $referencia));
        
        // Set your customer information.
        $payment->setSender()->setName('João Comprador');
        $payment->setSender()->setEmail('c84811440811966527340@sandbox.pagseguro.com.br');
        $payment->setSender()->setPhone()->withParameters(
            11,
            56273440
        );
        $payment->setSender()->setDocument()->withParameters(
            'CPF',
            '41409593800'
        );
        
        $payment->setShipping()->setAddress()->withParameters(
            'Av. Brig. Faria Lima',
            '1384',
            'Jardim Paulistano',
            '01452002',
            'São Paulo',
            'SP',
            'BRA',
            'apto. 114'
        );
        $payment->setShipping()->setCost()->withParameters(20.00);
        $payment->setShipping()->setType()->withParameters(\PagSeguro\Enum\Shipping\Type::SEDEX);
        
        //Add metadata items
        $payment->addMetadata()->withParameters('PASSENGER_CPF', '41409593800');
        $payment->addMetadata()->withParameters('GAME_NAME', 'DOTA');
        $payment->addMetadata()->withParameters('PASSENGER_PASSPORT', '23456', 1);
        
        //Add items by parameter
        //On index, you have to pass in parameter: total items plus one.
        $payment->addParameter()->withParameters('itemId', '0003')->index(3);
        $payment->addParameter()->withParameters('itemDescription', 'Notebook Amarelo')->index(3);
        $payment->addParameter()->withParameters('itemQuantity', '3')->index(3);
        $payment->addParameter()->withParameters('itemAmount', '200.00')->index(3);
        
        //Add items by parameter using an array
        $payment->addParameter()->withArray(['notificationURL', Route('api.notification.pagseguro.referencia', $referencia)]);
        
        $payment->setRedirectUrl(Route('checkout.pagseguro.pedido', $referencia));
        $payment->setNotificationUrl(Route('api.notification.pagseguro.referencia', $referencia));
        
        //Add discount
        $payment->addPaymentMethod()->withParameters(
            \PagSeguro\Enum\PaymentMethod\Group::CREDIT_CARD,
            \PagSeguro\Enum\PaymentMethod\Config\Keys::DISCOUNT_PERCENT,
            10.00 // (float) Percent
        );
        
        //Add installments with no interest
        $payment->addPaymentMethod()->withParameters(
            \PagSeguro\Enum\PaymentMethod\Group::CREDIT_CARD,
            \PagSeguro\Enum\PaymentMethod\Config\Keys::MAX_INSTALLMENTS_NO_INTEREST,
            2 // (int) qty of installment
        );
        
        //Add a limit for installment
        $payment->addPaymentMethod()->withParameters(
            \PagSeguro\Enum\PaymentMethod\Group::CREDIT_CARD,
            \PagSeguro\Enum\PaymentMethod\Config\Keys::MAX_INSTALLMENTS_LIMIT,
            6 // (int) qty of installment
        );
        
        // Add a group and/or payment methods name
        $payment->acceptPaymentMethod()->groups(
            \PagSeguro\Enum\PaymentMethod\Group::CREDIT_CARD,
            \PagSeguro\Enum\PaymentMethod\Group::BALANCE
        );
        // $payment->acceptPaymentMethod()->name(\PagSeguro\Enum\PaymentMethod\Name::DEBITO_ITAU);
        // Remove a group and/or payment methods name
        $payment->acceptPaymentMethod()->group(\PagSeguro\Enum\PaymentMethod\Group::BOLETO);
        // $payment->excludePaymentMethod()->group(\PagSeguro\Enum\PaymentMethod\Group::BOLETO);
        
        try {
            /**
             * @todo For checkout with application use:
             * \PagSeguro\Configuration\Configure::getApplicationCredentials()
             *  ->setAuthorizationCode("FD3AF1B214EC40F0B0A6745D041BF50D")
             */
            $result = $payment->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );

            // dd($result);
            return response()->redirectTo($result);
        
            echo "<h2>Criando requisi&ccedil;&atilde;o de pagamento</h2>"
                . "<p>URL do pagamento: <strong>$result</strong></p>"
                . "<p><a title=\"URL do pagamento\" href=\"$result\" target=\_blank\">Ir para URL do pagamento.</a></p>";
        } catch (Exception $e) {
            dd($e);
            die($e->getMessage());
        }
    }

    /******************
     * LIGHTBOX
     */
    public function checkoutLightbox()
    {
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        ?>
            <!DOCTYPE html>
            <html>
            <head>
                <?php if (\PagSeguro\Configuration\Configure::getEnvironment()->getEnvironment() == "sandbox") : ?>
                    <!--Para integração em ambiente de testes no Sandbox use este link-->
                    <script
                            type="text/javascript"
                            src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
                    </script>
                <?php else : ?>
                    <!--Para integração em ambiente de produção use este link-->
                    <script
                            type="text/javascript"
                            src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
                    </script>
                <?php endif; ?>
            </head>
            </html>
        
        <?php
        
        $payment = new \PagSeguro\Domains\Requests\Payment();
        
        $payment->addItems()->withParameters(
            '0001',
            'Notebook prata',
            2,
            130.00
        );
        
        $payment->addItems()->withParameters(
            '0002',
            'Notebook preto',
            2,
            430.00
        );

        // for($i=1; $i<8; $i++) {
        //     $payment->addItems()->withParameters(
        //         '000'.$i,
        //         'Item '.$i,
        //         ($i*2),
        //         5.00
        //     );
        // }

        $pedido = new Pedido([
            'codigo' => 'CHJPHP0000-'
        ]);
        $pedido->save();

        $pedido->codigo .= $pedido->id;
        $pedido->update();

        $referencia = $pedido->codigo;
        
        $payment->setCurrency("BRL");
        $payment->setReference($referencia);
        // $payment->setReference("LIBPHP000001");
        
        // $payment->setRedirectUrl("http://www.lojamodelo.com.br");
        $payment->setRedirectUrl(Route('checkout.pagseguro.pedido', $referencia));
        
        // Set your customer information.
        $payment->setSender()->setName('João Comprador');
        $payment->setSender()->setEmail('c84811440811966527340@sandbox.pagseguro.com.br');
        $payment->setSender()->setPhone()->withParameters(
            11,
            56273440
        );
        $payment->setSender()->setDocument()->withParameters(
            'CPF',
            '41409593800'
        );
        
        $payment->setShipping()->setAddress()->withParameters(
            'Av. Brig. Faria Lima',
            '1384',
            'Jardim Paulistano',
            '01452002',
            'São Paulo',
            'SP',
            'BRA',
            'apto. 114'
        );
        $payment->setShipping()->setCost()->withParameters(20.00);
        $payment->setShipping()->setType()->withParameters(\PagSeguro\Enum\Shipping\Type::SEDEX);
        
        // //Add metadata items
        $payment->addMetadata()->withParameters('PASSENGER_CPF', '41409593800');
        $payment->addMetadata()->withParameters('GAME_NAME', 'DOTA');
        $payment->addMetadata()->withParameters('PASSENGER_PASSPORT', '23456', 1);
        
        //Add items by parameter
        $payment->addParameter()->withParameters('itemId', '0003')->index(3);
        $payment->addParameter()->withParameters('itemDescription', 'Notebook Rosa')->index(3);
        $payment->addParameter()->withParameters('itemQuantity', '1')->index(3);
        $payment->addParameter()->withParameters('itemAmount', '201.40')->index(3);

        //Add items by parameter using an array
        $payment->addParameter()->withArray(['notificationURL', route('api.notification.pagseguro.referencia', $referencia)]);
        
        
        $payment->setRedirectUrl(Route('checkout.pagseguro.pedido', $referencia));
        $payment->setNotificationUrl(route('api.notification.pagseguro.referencia', $referencia));
        
        try {
            $onlyCheckoutCode = true;
            $result = $payment->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $onlyCheckoutCode
            );
        
            echo "<h2>Criando requisi&ccedil;&atilde;o de pagamento. Aguarde...</h2>"
                . "<p>C&oacute;digo da transa&ccedil;&atilde;o: <strong>" . $result->getCode() . "</strong></p>"
                . "<script>PagSeguroLightbox('" . $result->getCode() . "');</script>";
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}