<?php

namespace App\Http\Controllers\PagSeguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pedido;

class PlanoController extends Controller
{
    public function geraPlano()
    {
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        $preApproval = new \PagSeguro\Domains\Requests\PreApproval();
        
        // Set the currency
        $preApproval->setCurrency("BRL");
        
        // Set a reference code for this payment request. It is useful to identify this payment
        // in future notifications.
        $pedido = new Pedido([
            'codigo' => 'CHJPHP0000-'
        ]);
        $pedido->save();

        $pedido->codigo .= $pedido->id;
        $pedido->update();

        $referencia = $pedido->codigo;
        $preApproval->setReference($referencia);
        // $preApproval->setReference("REF123");
        
        // Set shipping information for this payment request
        $preApproval->setShipping()->setType(\PagSeguro\Enum\Shipping\Type::SEDEX);
        $preApproval->setShipping()->setAddress()->withParameters(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );
        
        // Set your customer information.
        $preApproval->setSender()->setName('João Comprador');
        $preApproval->setSender()->setEmail('c84811440811966527340@sandbox.pagseguro.com.br');
        $preApproval->setSender()->setPhone()->withParameters(
            11,
            56273440
        );
        
        $preApproval->setSender()->setAddress()->withParameters(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );
        
        /***
         * Pre Approval information
         */
        $preApproval->setPreApproval()->setCharge('auto'); //manual ou auto
        $preApproval->setPreApproval()->setName("Assinatura 5 usuarios");
        $preApproval->setPreApproval()->setDetails("Todo dia 30 será cobrado o valor de R100,00 referente ao Plano
                                de 5 Assinaturas OSD.");
        $preApproval->setPreApproval()->setAmountPerPayment('100.00');
        // $preApproval->setPreApproval()->setMaxAmountPerPeriod('200.00');
        $preApproval->setPreApproval()->setPeriod('Monthly');
        // $preApproval->setPreApproval()->setMaxTotalAmount('2400.00');
        // $preApproval->setPreApproval()->setInitialDate('2019-09-12T00:00:00');
        // $preApproval->setPreApproval()->setFinalDate('2019-12-12T00:00:00');
        
        $preApproval->setRedirectUrl(route('checkout.pagseguro.pedido', $referencia));
        $preApproval->setReviewUrl(route('checkout.pagseguro.pedido', $referencia));
        
        try {
        
            /**
             * @todo For checkout with application use:
             * \PagSeguro\Configuration\Configure::getApplicationCredentials()
             *  ->setAuthorizationCode("FD3AF1B214EC40F0B0A6745D041BF50D")
             */
            $response = $preApproval->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
        
            echo "<h2>Criando requisi&ccedil;&atilde;o de assinatura</h2>"
                . "<p>URL da assinatura: <strong>$response</strong></p>"
                . "<p><a title=\"URL da assinatura\" href=\"$response\" target=\_blank\">Ir para URL da assinatura.</a></p>";
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
    }

    public function changePlano(Request $request, $plano)
    {
        // dd($plano);
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        $preApproval = new \PagSeguro\Domains\Requests\PreApproval\Charge();
        // $preApproval->setReference("LIB0000001PREAPPROVAL");
        $preApproval->setReference("REF123");
        // $preApproval->setCode("30B5FFD8F2F2370224519FBDCC2BCA60");
        $preApproval->setCode($plano);
        $preApproval->addItems()->withParameters(
            '0001',
            'Notebook prata',
            1,
            100.00
        );
        
        try {
            $response = $preApproval->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function cancelPlano(Request $request, $plano)
    {
        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");
        
        /**
         * @var string PreApproval code
         */
        // $code = "DF7EB0AC9999333CC4379F82114239AB";
        $code = $plano;
                 
        
        try {
            $response = \PagSeguro\Services\PreApproval\Cancel::create(
                \PagSeguro\Configuration\Configure::getAccountCredentials(),
                $code
            );
        
            echo "<pre>";
            print_r($response);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
