<?php

namespace App\Http\Controllers\PagSeguro;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pedido;
use Exception;

class NotificationController extends Controller
{
    public function pedido(Request $request)
    {
        $referencia = $request['referencia'];

<<<<<<< HEAD
        $pedido = Pedido::where('codigo', $referencia)->firstOrfail();
        // $pedido->transaction_id = $request['transaction_id'];
        $pedido->update();
=======
        $pedido = Pedido::where('codigo', $referencia)->firstOrfail();  
        dd($pedido);
>>>>>>> 7cd148a8f7dc66010bd5a8a39095446549971c46
        
        return response()->json([$pedido], 200);

        // return 'pedido: ' . $request['transaction_id'];
    }

    public function referencia(Request $request)
    {
        try {
            \PagSeguro\Library::initialize();
            \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
            \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

            $credenciais = \PagSeguro\Configuration\Configure::getAccountCredentials();

            $notification_code = $request['notificationCode'];
            $email = $credenciais->getEmail();
            $token = $credenciais->getToken();

            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', "https://ws.sandbox.pagseguro.uol.com.br/v3/transactions/notifications/$notification_code?email=$email&token=$token");
            
            if($response->getStatusCode() == 200) {

                $xml = simplexml_load_string($response->getBody());
                $json = json_encode($xml);
                $array = json_decode($json,TRUE);
                // dd('SUSCESSO', $array);

                $pedido = Pedido::where('codigo', $request['referencia'])->firstOrFail();
                $pedido->notification_type = $request['notificationType'] ?? '';
                $pedido->notification_code = $request['notificationCode'] ?? '';
                $pedido->status_id = $array['status'] ?? '0';
                $pedido->transaction_id = $array['code'] ?? '0';
                $pedido->update();
            
                return response()->json([
                    'msg' => 'ok',
                    'qtd_pedidos' => Pedido::all()->count(),
                    'status' => route('status.pagseguro.reference', $pedido->codigo),
                    'pedido' => $pedido
                ], 201);

            } else {

                reponse()->json([
                    'error_cod' => $response->getStatusCode(),
                    'error_msg' => 'Error ao buscar notification'
                ], $response->getStatusCode());

            }
        }
        catch (Exception $e) {
            return response()->json([
                'error_cod' => $e->getCode(),
                'error_msg' => $e->getMessage()
            ], 500);
        }
    }
}
