<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'codigo', 'notification_code', 'notification_type', 'transaction_id', 'status_id'
    ];
}
