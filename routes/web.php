<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('pedidos', 'PedidoController');

Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function() {

    Route::group(['prefix' => 'pagseguro', 'as' => 'pagseguro.'], function() {

        Route::get('/normal', 'PagSeguro\CheckoutController@checkoutRedirect')->name('normal');
        Route::get('/lightbox', 'PagSeguro\CheckoutController@checkoutLightbox')->name('lightbox');
        Route::get('/pedido/{referencia}', 'PagSeguro\NotificationController@pedido')->name('pedido');
    });

});


Route::group(['prefix' => 'plano', 'as' => 'plano.'], function() {

    Route::group(['prefix' => 'pagseguro', 'as' => 'pagseguro.'], function() {

        Route::get('/gerar', 'PagSeguro\PlanoController@geraPlano')->name('gerar');
        Route::get('/{plano}/alterar', 'PagSeguro\PlanoController@changePlano')->name('change');
        Route::get('/{plano}/cancelar', 'PagSeguro\PlanoController@cancelPlano')->name('cancela');
    });

});

Route::group(['prefix' => 'status', 'as' => 'status.'], function() {

    Route::group(['prefix' => 'pagseguro', 'as' => 'pagseguro.'], function() {
        Route::get('/reference/{reference}', 'PagSeguro\StatusController@reference')->name('reference');
        Route::get('/transaction/{transaction}', 'PagSeguro\StatusController@transaction')->name('transaction');

    });

});